--Comment
--[[Multiline comment
--]]
print("Hello World")
aname="Alain"
io.write("Size:", #aname,"\n")
age=4
io.write("Age:",age,"\n")
io.write("Type:",type(age),"\n")
f=4.5
longstring=[[
I'm long
]]
io.write(longstring,"\n")
first="Alain"
last="De Vos"
full=first..last
io.write(full,"\n")
a=true
b=false
io.write("sqrt(64):",math.sqrt(64),"\n")
print(string.format("Pi=%.10f",math.pi))

age=13
if age == 16 then
		local mylocalvar=16
		io.write(mylocalvar,"\n")
		io.write("16\n")
elseif age ~= 16 then
		io.write("not 16\n")
else
		io.write("Error\n")
end

print(string.format("%s",tostring(not true)))

canvote = age > 18 and true or false  --Ternary operator


longstring="alaindevosalaindevosalaindevosalaindevos"
io.write("stringlen:",#longstring,"\n")
b=string.gsub(longstring,"a","b")
io.write(b,"\n")
index=string.find(longstring,"devos")
io.write("Index:",index,"\n")

--while
i=1
while(i<=10) do
	io.write(i,"\n")
	i=i+1
	if i==8 then break end
end

--do-while
i=1
repeat
	io.write(i,"\n")
	i=i+1
until i==5

for i=1,31,10 do
	io.write(i,"\n")
end

--Array
months={"Jan","Feb","Maa","Apr","Mei"}
for key,value in pairs(months) do
	io.write(value,"\n")
end

--Table
atable={}
for i=1,10 do
	atable[i]=3*i
end
io.write("First:",atable[1],"\n")
io.write("Length:",#atable,"\n")
table.insert(atable,3,7)
myconcat=table.concat(atable,",")
table.remove(atable,3)

--Multidim
am={}
for i=0,9 do
	am[i]={}
	for j=0,9 do
		am[i][j]=tostring(i)..tostring(j)
	end
end
io.write(am[1][2],"\n")

--function
function sum(a,b)
	return a+b
end
print(string.format("5+2=%d\n",sum(5,2)))
function zero_one()
	return 0,1
end
a,b=zero_one()
doubleit=
	function(x)
		return x*2
	end
io.write(doubleit(4),"\n")

--coroutine
co=coroutine.create(
	function()
		for i=1,10,1 do
			io.write(i,"\n")
			print(coroutine.status(co))
			if i==5 then
				coroutine.yield()
			end
		end
	end)
print(coroutine.status(co))
coroutine.resume(co)
print(coroutine.status(co))

--file
file=io.open("test.txt","a+")
file:write("a text")
file:seek("set",0)
x=file:read("*a")
print(x)
file:close()

--object
Person={firstname="",lastname=""}

function Person:new(firstname,lastname)
	self.firstname=firstname
	self.lastname=lastname
	return self
end

function Person:toString()
	fullname=string.format("%s %s\n",self.firstname,self.lastname)
	return fullname
end

eddy=Person:new("Eddy","Devos")
print(eddy.firstname)
print(eddy:toString())

Teacher=Person:new()

function Teacher:new(firstname,lastname,course)
	self.firstname=firstname
	self.lastname=lastname
	self.course=course
	return self
end

function Teacher:toString()
	fullname=string.format("%s %s %s\n",self.firstname,self.lastname,self.course)
	return fullname
end

einstein=Teacher:new("Albert","Einstein","Relativity")
print(einstein:toString())
